#!/bin/env python

# Simple script to get the date using the pendulum python package

# Import required packages
import pendulum
import sys


class IntelTask:

    def __init__(self, dir="/tmp/"):
        self.base_directory = dir

    #--------------------------------------------------------------------------
    def get_version(self):
        """
        Get the current version of python
        """
        py_version = str(sys.version_info[0]) + "." + \
            str(sys.version_info[1]) + "." + \
            str(sys.version_info[2])
        print("The current python version is {}".format(py_version))

#        fileName = self.base_directory + "python_version"
#        with open(fileName, 'w') as fPtr:
#            fPtr.write("The current python version is {}".format(py_version))

    #--------------------------------------------------------------------------
    def get_time(self):
        """
        Get the time
        """

        print(str(pendulum.now('Europe/Paris')))


# Execute the functions
if __name__ == "__main__":

    task = IntelTask()

    task.get_time()
