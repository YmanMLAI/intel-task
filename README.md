Intel Task 26/9/2021
====================

**Description of task**


This task is to schedule following python source code to be able to execute on multiple nodes:
1. Get the python version using:

        python -V

2. Get the date/time using the pendulum python module:

        import pendulum
        print(str(pendulum.now('Europe/Paris')))


- It must be able to be deployed and execute on a physical, virtual machine, docker container or kubernetes pod.
- It must be able execute on 2 versions of python - 2.7 and 3.9
- It must save the output of the 2 commands to 2 different files - 'stdout.txt' and 'version.txt' and then copy those
   files back. I have made 1 change to this requirement, detailed below.


**Details**


The task has been implemented in an ansible playbook. To execute, run the following command:

    ansible-playbook -i hosts.ini intel_ansible.yml --extra-vars "host=intel_servers" --connection=local

To see more output you can add '-vvv' to the command line arguments.

This should execute the tasks listed in the 'intel_ansible.yml' file on the list of servers associated with the
'server_list' variable in the 'hosts.ini' file.  What is in gitlab is only default configuration, so you will need to
change or add a list of servers that you want those commands to execute on, to the .ini file. You can change the list
in the [server_list] section or add your own.

Note:
- It will default to creating a local 'task_results' directory where it will place the 2 files mentioned above.
    The detault can be changed by specifying the directory on the command line by adding 'results_dir=<your directory>' to the 'extra-vars' argument, for example:

        ansible-playbook -i hosts.ini intel_ansible.yml --extra-vars "host=intel_servers results_dir=test-results" --connection=local

- It will check and install the python pendulum package on those servers if it does not exist.

The '--connection=local' is to avoid using ssh keys (the default behaviour) to connect to the servers.

There are 2 tasks in the ansible playbook:

1. The first is to get the python version.
2. The second is to execute the given time command from the task description above.

The output of both will be written to files in the local directory. I have added the name of the server it is executing
on to the required output file names stated above, to prevent entries being overwritten, for example:

    127.0.0.3.stdout.txt
    127.0.0.3.version.txt

The file 'intel_task.py' is the script that executes the pendulum time command.

**Assumptions/Limitations**

- The nodes are accessible via an ip address that can be specified in the 'hosts.ini' file.

- The hosts associated with 'server_list' are a flavor of UNIX and that pip is already installed.

- I have interpreted the 2 different versions of python stated in the task as a requirement that they are able to
execute on those versions and not to separately install them if they are not present.

- Testing has been limited to my local environment and the '--connection=local' was only used to make testing easier
by not having to uss ssh to connect to it.

- I had some issues with using the full module name from ansible (see https://github.com/ansible/ansible/issues/72627)
with the version I was using (2.9.6), so I used the shorter 'shell' name instead of 'ansible.builtin.shell'.
